from django.contrib import admin

from .forms import ProfileForm
from .models import AdminUser


@admin.register(AdminUser)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'external_id', 'phone')
    form = ProfileForm

