from rest_framework import serializers
from .models import AdminUser
from django import forms
class MessageSerializer(serializers.ModelSerializer):
  class Meta:
    model = AdminUser
    fields = ('id', 'external_id', 'phone')
