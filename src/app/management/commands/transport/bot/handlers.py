from aiogram import types,Dispatcher
from app.management.commands.intermediate import bot
from app.management.commands.services.database import verify_id,verify_phone,set_phone,get_phone,set_id
from app.management.commands.services.kb import default_kb, back_kb

async def start_handler (message: types.message):    
    ID= message.from_user.id

    if not verify_id(ID):
        set_id(ID)
        await message.reply(f'Привет, {message.from_user.full_name}\nЧтобы ввести данные напишите: /set_phone <номер телефона>')
    else:
        await message.reply(f'Привет, {message.from_user.full_name}',reply_markup=default_kb)
    

async def set_phone_handler (message: types.message):
    ID= message.from_user.id
    phone=message.text.split()[1]
    if len(phone)==11:
        set_phone(ID,phone)
        await message.reply(f' {message.from_user.full_name}, ваш телефон сохранён!',reply_markup=default_kb)
    elif verify_phone(ID):
        await message.reply('Проверьте правильность введённых данных',reply_markup=back_kb)
    else:
        await bot.send_message(ID,f'{message.from_user.full_name}, проверьте правильность введённых данных')
        
            
            
async def back_handler (message: types.message):
    ID=message.from_user.id
    if verify_phone(ID):
        await message.reply('Ввод данных отменён',reply_markup=default_kb)

async def get_handler (message: types.message):
    ID= message.from_user.id
    if verify_phone(ID):
        await bot.send_message(ID,f"""Привет, {message.from_user.full_name} Вот ваши данные:
Номер телефона: {get_phone(ID)} """)
    else:
        await message.reply('Для начала введите корректные данные')
async def endpoint_handler(message: types.message):
    ID= message.from_user.id
    if verify_phone(ID):
        await message.reply (f"https://api.telegram.org/bot5847205147:AAFYEtCU9HOarbC9nQK0fy2-bvlEfvVQyOU/sendMessage?chat_id={ID}&text={get_phone(ID)}")
    else:
        await message.reply('Для начала введите корректные данные')
 

        



  

def register_handler(dp:Dispatcher):
    dp.register_message_handler(back_handler, commands='Назад')
    dp.register_message_handler(start_handler, commands='start')
    dp.register_message_handler(set_phone_handler, commands='set_phone')
    dp.register_message_handler(get_handler, commands='me')
    dp.register_message_handler(endpoint_handler, commands='me_endpoint')


    
