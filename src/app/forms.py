from django import forms

from .models import AdminUser


class ProfileForm(forms.ModelForm):

    class Meta:
        model = AdminUser
        fields = (
            'external_id',
            'phone',
        )
        widgets = {
            'phone': forms.TextInput,
        }
