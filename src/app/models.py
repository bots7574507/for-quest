from django.db import models
from django.core.validators import MaxValueValidator , MinValueValidator


class AdminUser(models.Model):
    external_id = models.PositiveIntegerField(
        verbose_name='Внешний ID пользователя',
        unique=True,
    )
    phone = models.IntegerField(
        verbose_name='Номер пользователя',
        null=True,
        blank=True,
        validators=[
            MaxValueValidator(99999999999),
            MinValueValidator(10000000000),

            
        ],
        
    )

    def __str__(self):
        return str(self.phone)

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'
        db_table='users'



