
from django.shortcuts import render

from rest_framework import viewsets
from .models import AdminUser
from .serializers import MessageSerializer

class MessageViewSet(viewsets.ModelViewSet):
  queryset = AdminUser.objects.all()
  serializer_class = MessageSerializer
# Create your views here.
